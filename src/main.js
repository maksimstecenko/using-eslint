import {
  appInputFormJoinOurProgram,
  appInputFormInputEmailJoinOurProgram,
  createSectionJoinOurProgram,
} from './join-us-section.js';

import { validate } from './email-validator.js';// eslint-disable-next-line
                     
/* const htmlBody = document.getElementById("app-container") */

document.addEventListener('DOMContentLoaded', createSectionJoinOurProgram());

appInputFormJoinOurProgram.addEventListener('submit', (event) => {
  event.preventDefault();
  const emailValue = appInputFormInputEmailJoinOurProgram.value;

  console.log(emailValue);

  console.log(`Entered email: ${emailValue}`);
});

const emailInput = document.querySelector('.app-form-input-email_join-our-program');
const submitButton = document.querySelector('.app-form-input-submit_join-our-program');

submitButton.addEventListener('click', () => {
  const email = emailInput.value;
  if (validate(email)) {
    alert('Email is valid');
  } else {
    alert('Wrong email');
  }
});
